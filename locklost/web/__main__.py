import os
from datetime import datetime

import bottle

from .. import config
from ..event import LocklossEvent, find_events
from . import utils

##################################################

template_path = os.path.join(os.path.dirname(__file__), 'templates')
bottle.TEMPLATE_PATH.insert(0, template_path)

# NOTE: shouldn't be needed, but url paths include this from apache
WEB_SCRIPT = config.WEB_ROOT + '/index.cgi'

##################################################

app = bottle.Bottle()

def event_summary(gps):
    try:
        event = LocklossEvent(gps)
    except (ValueError, OSError) as e:
        bottle.abort(404, 'Unknown event: {}'.format(gps))

    else:
        try:
            online_status = utils.online_status()
        except OSError: ### no online jobs
            online_status = None

        sat_channels = []
        csv_path = event.path('saturations.csv')
        if os.path.exists(csv_path):
            with open(csv_path, 'r') as f:
                for line in f:
                    sat_channels.append(line.split())

        return bottle.template(
            'event.tpl',
            IFO=config.IFO,
            web_script=WEB_SCRIPT,
            date=datetime.now(),
            is_home=False,
            event=event,
            tags=utils.tag_buttons(event.list_tags()),
            sat_channels=sat_channels,
            online_status=online_status,
        )

##################################################

@app.route("/")
@app.route("/tag/<tag>")
def index(tag='all'):

    if bottle.request.query.format and bottle.request.query.format == 'json':
        if 'event' in bottle.request.query:
            gps = bottle.request.query.get('event')
            try:
                event = LocklossEvent(gps)
            except (ValueError, OSError) as e:
                bottle.abort(404, {'error': {'code': 404, 'message': 'Unknown event: {}'.format(gps)}})
            return event.to_dict()

        query = utils.query_to_dict(dict(bottle.request.query))
        events = []
        for i, event in enumerate(find_events(**query)):
            if bool(query.get('limit', 0)) and i > query['limit']:
                break
            events.append(event.to_dict())
        return {'events': events}

    elif 'event' in bottle.request.query:
        gps = bottle.request.query.get('event')
        return event_summary(gps)

    else:
        query = utils.query_to_dict(dict(bottle.request.query))

        if not query:
            is_home = True
        else:
            is_home = False

        # limit events shown if date range hasn't been specified
        if not ('after' in query and 'before' in query) \
           and not 'limit' in query:
            query['limit'] = config.QUERY_DEFAULT_SHOW

        try:
            online_status = utils.online_status()
        except OSError: ### no online jobs
            online_status = None

        return bottle.template(
            'index.tpl',
            IFO=config.IFO,
            web_script=WEB_SCRIPT,
            date=datetime.now(),
            is_home=is_home,
            query=query,
            online_status=online_status,
        )


@app.route("/event/<gps>")
def event_route(gps):
    bottle.redirect('{}?event={}'.format(WEB_SCRIPT, gps))


@app.route("/tag/<tag>")
def tag_route(tag='all'):
    query = utils.query_to_str(dict(bottle.request.query))
    bottle.redirect('{}?tag={}&{}'.format(WEB_SCRIPT, tag, query))


@app.route("/api")
@app.route("/api/tag/<tag>")
def json_route(tag='all'):
    query = utils.query_to_str(dict(bottle.request.query))
    bottle.redirect('{}?format=json&{}'.format(WEB_SCRIPT, query))

##################################################

if __name__ == '__main__':
    bottle.run(app, server='cgi', debug=True)
