import os
from datetime import datetime

from .. import config

##################################################

def query_to_dict(query):
    """convert query to dictionary"""
    filter_val = {
        'after': int,
        'before': int,
        'state': str,
        'limit': int,
        'tag': str,
    }
    out_query = {}
    for key in filter_val.keys():
        if key in query and query[key]:
            out_query[key] = filter_val[key](query[key])
    return out_query


def query_to_str(query):
    """convert query to string"""
    return '&'.join(
        ['{}={}'.format(k, v) for k, v in query_to_dict(query).items()])


def online_status():
    """
    displays HTML-formatted online status information
    """
    stat_file = os.path.join(config.CONDOR_ONLINE_DIR, 'stat')
    if not os.path.exists(stat_file):
        return '<span style="color:red">ONLINE ANALYSIS NOT RUNNING</span>'
    stat = os.stat(stat_file)
    dt = datetime.fromtimestamp(stat.st_mtime)
    dtnow = datetime.now()
    age = dtnow - dt
    tsecs = age.total_seconds()
    if tsecs > 60:
        color = 'red'
    else:
        color = 'green'
    wcroot = os.path.join(config.WEB_ROOT, 'events', '.condor_online')
    return '<span style="color: {}">online last update: {:0.1f} min ago ({}) [<a href="{}">log</a>]</span>'.format(
        color,
        tsecs/60,
        dt,
        os.path.join(wcroot, 'out'),
    )


def analysis_status_button(event):
    """
    HTML for the event analysis status button
    """
    if event.analyzing:
        fflag = "analyzing"
        state = 'warning'
    elif event.analyzed:
        if event.analysis_succeeded:
            fflag = "analyzed [{}]".format(event.analysis_version)
            state = 'success'
        else:
            fflag = "fail"
            state = 'danger'
    else:
        fflag = "queued"
        state = 'warning'
    log_link = event.url('log')
    return '<a href="{log_link}" class="btn-sm btn-{state} btn-block" role="button" style="text-align:center;">{fflag}</a>'.format(
        log_link=log_link,
        state=state,
        fflag=fflag,
    )


def tag_buttons(tags):
    """HTML for tags"""
    buttons = []
    for tag in tags:
        colors = config.TAG_COLORS.get(tag, ('white', 'grey'))
        buttons.append(
            '<span style="color:{}; background-color:{}">{}</span>'.format(
                colors[0], colors[1], tag))
    return ' '.join(buttons)


def event_plot_urls(event, plot):
    """
    Finds the urls corresponding to an event plot.
    """
    plot_urls = []
    for zoom_level in ['WIDE', 'ZOOM']:
        plot_name = '{}_{}.png'.format(plot, zoom_level)
        plot_path = event.path(plot_name)
        if os.path.exists(plot_path):
            plot_urls.append(event.url(plot_name))
    return plot_urls


def gen_thumbnail_url(plot_url):
    """ Convert plot url to thumbnail url """
    url_start = '/'.join(plot_url.split('/')[0:-1])
    url_end = plot_url.split('/')[-1].split('.')[0]+'_thumbnail.png'
    return url_start+'/thumbnails/'+url_end
