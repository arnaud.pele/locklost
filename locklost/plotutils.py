import matplotlib.pyplot as plt


def set_thresh_crossing(ax, thresh_crossing, gps, segment):
    """Plots and annotates vertical position of threshold crossing.

    """
    ax.axvline(
        thresh_crossing-gps,
        linestyle='--',
        color='firebrick',
        lw=5,
    )
    x_fraction = (thresh_crossing-segment[0])/(segment[1]-segment[0])
    ax.annotate(
        'First threshold crossing',
        xy=(x_fraction, 1),
        xycoords='axes fraction',
        horizontalalignment='center',
        verticalalignment='bottom',
        bbox=dict(boxstyle="round", fc="w", ec="red", alpha=0.95),
    )


def set_rcparams():
    """Standard matplotlib plot styling.

    """
    plt.rcParams['font.size'] = 30
    plt.rcParams['axes.titlesize'] = 40
    plt.rcParams['axes.titlepad'] = 15
    plt.rcParams['axes.labelsize'] = 30
    plt.rcParams['xtick.labelsize'] = 30
    plt.rcParams['ytick.labelsize'] = 30
    plt.rcParams['legend.fontsize'] = 30
    plt.rcParams['agg.path.chunksize'] = 1000
