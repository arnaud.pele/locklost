import os
import shutil
import logging

from gwpy.segments import Segment, SegmentList


def load_segments(segdir, intersect=None):
    """Load segments from directory

    """
    seglist = SegmentList()
    if not os.path.exists(segdir):
        return seglist
    i = -1
    for i, sf in enumerate(os.listdir(segdir)):
        s, e = map(int, sf.split('-'))
        seglist.append(Segment(s, e))
    logging.debug("{} segments loaded".format(i+1))
    return seglist.coalesce()


# def slice_segment(seg):
#     s0 = s1 = seg[0]
#     while s1 < seg[1]:
#         s1 = s0 + stride
#         if s1 > stop:
#             s1 = stop
#         yield Segment(s0, s1)
#         s0 = s1 - overlap
#     nseg = None


def slice_segments(seglist, stride, overlap=0):
    """Slice segment list into length segments

    """
    for seg in seglist.coalesce():
        start = seg[0]
        end = min(seg[0]+stride, seg[1])
        nseg = Segment(start, end)
        yield nseg.protract(overlap)
        while nseg[1] <= seg[1]:
            nseg = nseg.shift(stride)
            yield nseg.protract(overlap)


def write_segments(segdir, seglist):
    """Write segments to directory

    Returns number of segments written.

    """
    try:
        os.makedirs(segdir)
    except OSError:
        pass
    for i, seg in enumerate(seglist):
        sf = '-'.join(map(str, seg))
        open(os.path.join(segdir, sf), 'w').close()
    return i+1


def compress_segdir(segdir):
    """Compress segment directory with coalesce

    """
    if not os.path.exists(segdir):
        #raise RuntimeError("segdir does not exist")
        logging.warning("segdir does not exist")
        return
    tmpdir = segdir+'.tmp'
    if os.path.exists(tmpdir):
        #raise RuntimeError("segdir compression in progress")
        logging.warning("segdir compression in progress, skipping")
        return
    logging.info("compressing segdir: {}".format(segdir))
    shutil.move(segdir, tmpdir)
    n = write_segments(segdir, load_segments(tmpdir))
    logging.debug("{} segments written".format(n))
    shutil.rmtree(tmpdir)
