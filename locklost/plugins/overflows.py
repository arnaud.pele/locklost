import logging
import itertools
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment, SegmentList
from gwpy.segments import DataQualityFlag, DataQualityDict

from .. import config
from .. import data

#################################################

def find_overflows(event):
    """Create ADC overflow plots centered around the lock loss time.

    """
    gps = event.gps

    # set plot rcparams for this plugin
    plt.rcParams['text.usetex'] = False
    plt.rcParams['axes.titlesize'] = 16
    plt.rcParams['axes.labelsize'] = 16
    plt.rcParams['xtick.labelsize'] = 16
    plt.rcParams['ytick.labelsize'] = 16

    logging.info('acquiring ADC overflow data')
    segment = Segment(config.PLOT_WINDOWS['WIDE']).shift(gps)
    all_bufs = data.fetch(generate_all_overflow_channels(), segment)

    # generate plots
    overflows_overview = DataQualityDict()
    for window_type, window in config.PLOT_WINDOWS.items():
        segment = Segment(window).shift(gps)

        for subsystem in config.ADC_OVERFLOWS.keys():
            logging.info('plotting {} ADC overflows for {} subsystem'.format(
                window_type,
                subsystem,
            ))
            for bit1 in range(config.ADC_OVERFLOWS[subsystem]['num_bits']):
                adc_id = config.ADC_OVERFLOWS[subsystem]['ADC_ID']
                channels = generate_overflow_channels(subsystem, bit1)
                bufs = [buf for buf in all_bufs if buf.channel in channels]
                bufs.sort(key = lambda x: x.channel)

                srate = bufs[0].sample_rate
                dt = 1/srate
                t = np.arange(window[0], window[1], dt)

                # find overflows and make a DQ flag for each channel
                overflows = DataQualityDict()
                for buf in bufs:
                    y, t = buf.yt()
                    idx_overflow = np.argwhere(y != 0)
                    t_overflow = list(itertools.chain(*t[idx_overflow].tolist()))
                    overflow_segs = SegmentList([Segment(t, t+dt) for t in t_overflow])
                    _, bit2 = buf.channel.rsplit('_', 1)
                    bit_name = 'bit {}'.format(str(bit2).zfill(2))
                    overflows[bit_name] = DataQualityFlag(
                        name=bit_name,
                        active=overflow_segs,
                        known=SegmentList([segment]),
                    )

                fig = overflows.plot(
                    facecolor='darkred',
                    edgecolor='darkred',
                    known={'alpha': 0.2, 'facecolor': 'lightgray', 'edgecolor': 'gray'},
                    figsize=[12, 16],
                    label='name',
                )

                # set plot settings + title
                fig.tight_layout(pad=0.1)
                fig.subplots_adjust(bottom=0.4, top=0.8)

                ax = fig.gca()
                ax.set_xscale('seconds', epoch=gps)
                ax.grid(False)
                ax.set_title('ADC Overflows for {} (FEC ID: {}) for ADC number {}'.format(
                    subsystem,
                    adc_id,
                    bit1,
                ))

                # add vertical line to mark lock loss time
                ax.axvline(
                    gps,
                    color='black',
                    linestyle='--',
                    lw=1,
                )

                outfile_plot = 'adc_overflow_{}_{}_{}.png'.format(
                    subsystem,
                    bit1,
                    window_type,
                )
                outpath_plot = event.path(outfile_plot)
                fig.savefig(outpath_plot, bbox_inches='tight')
                fig.clf()

                ### aggregate overflows for a specific ADC together
                union_name = '{}: ADC number {}'.format(subsystem, bit1)
                overflows_overview[union_name] = overflows.union()
                overflows_overview[union_name].name = union_name


        ### overview overflow plot
        logging.info('plotting {} ADC overflows overview'.format(window_type))
        fig = overflows_overview.plot(
            facecolor='darkred',
            edgecolor='darkred',
            known={'alpha': 0.2, 'facecolor': 'lightgray', 'edgecolor': 'gray'},
            figsize=[12, 16],
            label='name',
        )

        fig.tight_layout(pad=0.1)
        fig.subplots_adjust(bottom=0.4, top=0.8)

        # set plot settings + title
        ax = fig.gca()
        ax.set_xscale('seconds', epoch=gps)
        ax.grid(False)
        ax.set_title('ADC Overflows Overview')

        # add vertical line to mark lock loss time
        ax.axvline(
            gps,
            color='black',
            linestyle='--',
            lw=1,
        )

        outfile_plot = 'adc_overflow_overview_{}.png'.format(window_type)
        outpath_plot = event.path(outfile_plot)
        fig.savefig(outpath_plot, bbox_inches='tight')
        fig.clf()


OVERFLOW_TEMPLATE='{}:FEC-{}_ADC_OVERFLOW_{}_{}'
def generate_overflow_channels(subsystem, bit1):
    ifo = config.IFO
    adc_id = config.ADC_OVERFLOWS[subsystem]['ADC_ID']
    bit_exclude = config.ADC_OVERFLOWS[subsystem]['bit_exclude']
    return [OVERFLOW_TEMPLATE.format(ifo, adc_id, bit1, bit2)
            for bit2 in range(32) if (bit1, bit2) not in bit_exclude]


def generate_all_overflow_channels():
    all_channels = []
    for subsystem in config.ADC_OVERFLOWS.keys():
        for bit1 in range(config.ADC_OVERFLOWS[subsystem]['num_bits']):
            all_channels.extend(generate_overflow_channels(subsystem, bit1))
    return all_channels
