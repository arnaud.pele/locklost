'''locklost followup analysis plugins

The package consists of functions to be run during a locklost event
followup analysis.  Plugin functions should take a LocklossEvent as
argument.

Plugins are registerd with the register_plugin() function.  Plugins
are currently executed sequentially in a single process, so register
ordering is preserved as a poor-man's dependency tree.

FIXME: figure out better way to express dependencies (e.g. DAG)

'''
import collections

import matplotlib.pyplot as plt
from matplotlib import rcParamsDefault
plt.rcParams.update(rcParamsDefault)


PLUGINS = collections.OrderedDict()
def register_plugin(func):
    PLUGINS.update([(func.__name__, func)])

from .discover import discover_data
register_plugin(discover_data)

from .refine import refine_time
register_plugin(refine_time)

from .observe import check_observe
register_plugin(check_observe)

from .history import find_previous_state
register_plugin(find_previous_state)

from .saturations import find_saturations
register_plugin(find_saturations)

from .lpy import find_lpy
register_plugin(find_lpy)

from .lsc_asc import plot_lsc_asc
register_plugin(plot_lsc_asc)

from .glitch import analyze_glitches
register_plugin(analyze_glitches)

from .overflows import find_overflows
register_plugin(find_overflows)

from .brs import check_brs
register_plugin(check_brs)

from .board_sat import check_boards
register_plugin(check_boards)

from .wind import check_wind
register_plugin(check_wind)

from .ads_excursion import check_ads
register_plugin(check_ads)

# add last since this needs to wait for additional data
from .seismic import check_seismic
register_plugin(check_seismic)
