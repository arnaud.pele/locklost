import logging
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

##############################################

def check_boards(event):
    """Checks for analog board saturations.

    Checks analog board channels for voltages above threshold and
    creates a tag/table if above a certain threshold.

    """
    plotutils.set_rcparams()

    mod_window = [config.BOARD_SEARCH_WINDOW[0], config.BOARD_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    board_channels = data.fetch(config.ANALOG_BOARD_CHANNELS, segment)

    saturating = False
    thresh_crossing = segment[1]
    for buf in board_channels:
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        before_loss = buf.data[np.where(t<event.gps-config.BOARD_SAT_BUFFER)]
        if any(abs(before_loss) >= config.BOARD_SAT_THRESH):
            saturating = True
            glitch_idx = np.where(abs(buf.data) > config.BOARD_SAT_THRESH)[0][0]
            glitch_time = t[glitch_idx]
            thresh_crossing = min(glitch_time, thresh_crossing)

    if saturating:
        event.add_tag('BOARD_SAT')
    else:
        logging.info('no saturating analog boards')

    fig, ax = plt.subplots(1, figsize=(22,16))
    for idx, buf in enumerate(board_channels):
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        ax.plot(
            t-event.gps,
            buf.data,
            label=buf.channel,
            alpha=0.8,
            lw=2,
            color=config.BOARD_SAT_CM[idx],
        )
    ax.axhline(
        config.BOARD_SAT_THRESH,
        linestyle='--',
        color='black',
        label='Board saturation threshold',
        lw=5,
    )
    ax.axhline(
        -config.BOARD_SAT_THRESH,
        linestyle='--',
        color='black',
        lw=5,
    )
    if thresh_crossing != segment[1]:
        plotutils.set_thresh_crossing(ax, thresh_crossing, event.gps, segment)

    ax.grid()
    ax.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax.set_ylabel('Voltage [V]')
    ax.set_ylim(-config.BOARD_SAT_THRESH-1, config.BOARD_SAT_THRESH+1)
    ax.set_xlim(t[0]-event.gps, t[-1]-event.gps)
    ax.legend(loc='best')
    ax.set_title('Analog board monitors', y=1.04)
    fig.tight_layout(pad=0.05)

    outfile_plot = 'board_sat.png'
    outpath_plot = event.path(outfile_plot)
    fig.savefig(outpath_plot, bbox_inches='tight')
