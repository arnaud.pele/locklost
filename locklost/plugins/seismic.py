import logging
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

##############################################

def check_seismic(event):
    """Check for elevated ground motion.

    Check the output of H1:ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON around the
    time of lockloss, plot the data, and create a tag if above a
    certain threshold.

    """
    mod_window = [config.SEI_SEARCH_WINDOW[0], config.SEI_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))

    # need to wait for low-frequency response data to be available
    # since it's later than the the default data discovery window
    # FIXME: what to do if timeout reached?
    data.data_wait(segment)

    seismic_channels = data.fetch(config.SEISMIC_CHANNELS, segment)

    if any(seismic_channels[0].data > config.SEI_THRESH):
        event.add_tag('SEISMIC')
    else:
        logging.info('ground motion below threshold')

    blrms_srate = seismic_channels[0].sample_rate
    blrms_t = np.arange(segment[0], segment[1], 1/blrms_srate)
    raw_srate = seismic_channels[1].sample_rate
    raw_t = np.arange(segment[0], segment[1], 1/raw_srate)

    plotutils.set_rcparams()

    fig, ax = plt.subplots(1, figsize=(22,16))
    ln1 = ax.plot(
        blrms_t-event.gps,
        seismic_channels[0].data,
        label=seismic_channels[0].channel,
        alpha=0.8,
        lw=2,
        color='indigo',
    )
    ax2 = ax.twinx()
    ln2 = ax2.plot(
        raw_t-event.gps,
        seismic_channels[1].data,
        label=seismic_channels[1].channel,
        alpha=0.6,
        lw=2,
        color='seagreen',
    )
    ln3 = ax.axhline(
        config.SEI_THRESH,
        linestyle='--',
        color='indigo',
        label='seismic threshold',
        lw=5,
    )
    # setting left y-axis paramters
    ax.spines['left'].set_color('indigo')
    ax.yaxis.label.set_color('indigo')
    ax.tick_params(axis='y', colors='indigo')
    ax.set_ylabel('Band-limited RMS Velocity [nm/s]')
    ax.set_ylim(0, max(seismic_channels[0].data)+1)

    # setting right y-axis parameters
    ax2.spines['right'].set_color('seagreen')
    ax2.yaxis.label.set_color('seagreen')
    ax2.tick_params(axis='y', colors='seagreen')
    ax2.set_ylabel('Raw Output Velocity [nm/s]')

    # setting general plot parameters
    lns = ln1+ln2+[ln3]
    labs = [l.get_label() for l in lns]
    ax.legend(lns, labs, loc='best')
    ax.set_xlabel(
        'Time [s] since lock loss at {}'.format(event.gps),
        labelpad=10,
    )
    plt.xlim(blrms_t[0]-event.gps, blrms_t[-1]-event.gps)
    ax.set_title('Z-axis seismic motion')

    fig.tight_layout(pad=0.05)
    outfile_plot = 'seismic.png'
    outpath_plot = event.path(outfile_plot)
    fig.savefig(outpath_plot, bbox_inches='tight')
