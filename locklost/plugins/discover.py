import glob
import time
import logging

from gwpy.segments import Segment, SegmentList

from .. import config
from .. import data

##################################################

def find_shm_segments():
    """Returns a segment list covering the span of available frame data from /dev/shm.

    """
    from lal.utils import CacheEntry
    frames = glob.glob("/dev/shm/lldetchar/{}/*".format(config.IFO))
    cache = map(CacheEntry.from_T050017, frames)
    return SegmentList([e.segment for e in cache]).coalesce()


def discover_data(event):
    """Discovers sources of data for other plugins.

    If data is not available will wait until there is up to a maximum
    time specified.

    """
    # use transition gps since it doesn't depend on refinement
    gps = event.transition_gps

    ### use refine window to determine data query range
    # gwdatafind requires integer segment times
    query_segment = Segment(*config.REFINE_WINDOW).shift(int(gps))
    query_segs = SegmentList([query_segment])
    logging.info("querying for data in range: {} - {}".format(*query_segment))

    ### first check if low-latency frames are available
    logging.info("checking if data is available in /dev/shm...")
    try:
        shm_segs = find_shm_segments()
    except ImportError as e:
        logging.info("unable to check /dev/shm: {}".format(e))
        shm_segs = Segment()
    if query_segs & shm_segs == query_segs:
        shm_end_time = shm_segs[-1][1]
        query_end_time = query_segment[1]

        ### check if time requested is too new, if so,
        ### sleep until it is available
        if query_end_time > shm_end_time:
            time.sleep(query_end_time - shm_end_time)
            shm_segs = find_shm_segments()

        ### check if span of data requested is all available
        if query_segs & shm_segs == query_segs:
            logging.info("queried data available in /dev/shm")
            return True

    ### if shm frames are not available for the time requested,
    ### wait until raw frames from LDR are available
    logging.info("no data available in /dev/shm, checking LDR...")
    if data.data_wait(query_segment):
        logging.info("LDR data available")
        return

    raise RuntimeError("data discovery timeout reached, data not found")
