import logging
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from .. import data

##################################################

def plot_indicators(event, params, refined_gps=None, threshold=None):
    t0 = event.transition_gps
    trans = event.transition_index

    channels = [
        config.GRD_STATE_N_CHANNEL,
        params['CHANNEL'],
    ]
    window = config.REFINE_PLOT_WINDOWS['WIDE']
    segment = Segment(*window).shift(t0)
    bufs = data.fetch(channels, segment)

    y_grd, t_grd = bufs[0].yt()
    y_ind, t_ind = bufs[1].yt()

    plt.rcParams['text.usetex'] = False

    isc_loss_time = event.transition_gps - t0

    for window_type, window in config.REFINE_PLOT_WINDOWS.items():
        logging.info('Making {} indicator plot'.format(window_type))

        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        # this puts ax1 on top
        ax1.set_zorder(10)
        # sets ax1 background invisible so the lower axis shows through
        ax1.patch.set_visible(False)

        # plot indicator
        color = 'blue'
        label = params['CHANNEL']
        # filter data based on window
        condition = np.logical_and(t0+window[0] <= t_ind, t_ind <= t0+window[1])
        idx = np.argwhere(condition)
        y = y_ind[idx]
        t = t_ind[idx]
        ax1.plot(t-t0, y, label=label,
                 color=color, alpha=0.8, linewidth=2)
        ax1.grid(True)
        ax1.set_ylabel(params['CHANNEL'])
        ax1.yaxis.label.set_color(color)
        ax1.set_xlabel('Time [s] since {}'.format(t0))

        # add annotation/line for threshold
        if threshold is not None:
            ax1.axhline(
                threshold,
                label='threshold',
                color='green',
                linestyle='--',
                linewidth=2,
            )
            if params['THRESHOLD'] > 0:
                xyoffset = (8, 10)
                valign = 'bottom'
            else:
                xyoffset = (8, -10)
                valign = 'top'
            ax1.annotate(
                "threshold",
                xy=(ax1.get_xlim()[0], threshold),
                xycoords='data',
                xytext=xyoffset,
                textcoords='offset points',
                horizontalalignment='left',
                verticalalignment=valign,
                bbox=dict(
                    boxstyle="round", fc="w", ec="green", alpha=0.95),
                )

        # add line for refined time
        if refined_gps:
            annotation = "refined time: {}".format(refined_gps)
            atx = refined_gps - t0
            ax1.axvline(
                atx,
                label='refined time',
                color='red',
                alpha=0.8,
                linestyle='--',
                linewidth=2,
            )
        else:
            annotation = "unable to refine time"
            atx = 0
        ax1.annotate(
            annotation,
            xy=(atx, ax1.get_ylim()[1]),
            xycoords='data',
            xytext=(0, 2),
            textcoords='offset points',
            horizontalalignment='center',
            verticalalignment='bottom',
            bbox=dict(boxstyle="round", fc="w", ec="red", alpha=0.95),
            )

        # plot guardian
        color = 'orange'
        label = config.GRD_STATE_N_CHANNEL
        # filter data based on window
        condition = np.logical_and(t0+window[0] <= t_grd, t_grd <= t0+window[1])
        idx = np.argwhere(condition)
        y = y_grd[idx]
        t = t_grd[idx]
        ax2.plot(t-t0, y, label=label,
                 color=color, linewidth=2)
        ax2.set_yticks(trans)
        ylim = ax2.get_ylim()
        ax2.set_ylim([ylim[0]-10, ylim[1]+10])
        ax2.set_ylabel(config.GRD_STATE_N_CHANNEL)
        ax2.yaxis.label.set_color(color)

        ax2.axhline(
            trans[0],
            label = 'state before lockloss',
            color = 'orange',
            linestyle = '--',
            linewidth = 2
        )

        ax2.axvline(
            isc_loss_time,
            label = 'lockloss time',
            color = 'orange',
            linestyle = '--',
            linewidth = 2
        )

        fig.suptitle("Lock loss refinement for {}".format(event.id))

        outfile = 'indicators_{}.png'.format(window_type)
        outpath = event.path(outfile)
        logging.debug(outpath)
        fig.savefig(outpath)


def refine_time(event):
    """Refine lock loss event time

    """
    gps = event.transition_gps
    refined_gps = None

    # find indicator channel to use based on guardian state
    for index, params in sorted(config.INDICATORS.items(), reverse=True):
        if event.transition_index[0] >= index:
            break

    channels = [
        params['CHANNEL'],
    ]
    window = config.REFINE_WINDOW
    segment = Segment(*window).shift(gps)
    buf = data.fetch(channels, segment)[0]

    # calculate mean and std using first 5 seconds of window
    samples = int(buf.sample_rate * 5)
    lock_data = buf.data[:samples]
    lock_mean = np.mean(lock_data)
    lock_stdd = np.std(lock_data - lock_mean, ddof=1)
    logging.info("locked mean/stddev: {}/{}".format(lock_mean, lock_stdd))

    # lock loss threshold is when moves past % threshold of std
    threshold = lock_stdd * params['THRESHOLD'] + lock_mean
    if params['THRESHOLD'] > 0:
        threshold = min(threshold, max(buf.data))
    else:
        threshold = max(threshold, min(buf.data))
    logging.info("threshold: {}".format(threshold))

    # if the mean is less than the nominal full lock value then abort,
    # as this isn't a clean lock
    if lock_mean < params['MINIMUM']:
        logging.info("channel mean below minimum, unable to resolve time")

    else:
        if params['THRESHOLD'] > 0:
            inds = np.where(buf.data > threshold)[0]
        else:
            inds = np.where(buf.data < threshold)[0]

        if inds.any():
            ind = np.min(inds)
            refined_gps = buf.tarray[ind]
            logging.info("refined time: {}".format(refined_gps))

        else:
            logging.info("no threshold crossings, unable to resolve time")

    if not refined_gps:
        ts = 'nan'
    else:
        ts = '{:f}'.format(refined_gps)
        event.add_tag('REFINED')

    with open(event.path('refined_gps'), 'w') as f:
        f.write('{}\n'.format(ts))

    logging.info("plotting indicators...")
    plot_indicators(
        event,
        params,
        refined_gps=refined_gps,
        threshold=threshold,
    )
