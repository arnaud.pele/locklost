import os
import logging
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

#################################################

def find_lpy(event):
    """Create length, pitch, and yaw plots

    For first saturated channel centered around the lock loss time.

    """

    plotutils.set_rcparams()
    gps = event.gps

    # determine first suspension channel to saturate from saturation plugin
    infile_csv = 'saturations.csv'
    inpath_csv = event.path(infile_csv)
    sat_channel = ''
    lpy_lim = 4*config.SATURATION_THRESHOLD

    if not os.path.exists(inpath_csv):
        logging.info("LPY plot bypassed (no saturating suspensions).")
        return

    with open(inpath_csv, 'r') as f:
        first_sat = f.readline()
        if first_sat:
            sat_channel, sat_time = first_sat.split(' ', 1)

    # generate the LPY mapping and channels to query from base channel
    lpy_map = channel2lpy_coeffs(sat_channel)
    base_channel, _, _ = sat_channel.rsplit('_', 2)
    channels = [get_sus_channel(base_channel, corner) for corner in lpy_map['corners']]

    for window_type, window in config.PLOT_WINDOWS.items():
        logging.info('Making {} LPY plot for {}'.format(window_type, base_channel))

        segment = Segment(window).shift(gps)
        bufs = data.fetch(channels, segment)

        srate = bufs[0].sample_rate
        t = np.arange(window[0], window[1], 1/srate)

        # calculate LPY using mapping from channels to coeffs
        titles = ['length', 'pitch', 'yaw']
        lpy = defaultdict(lambda: np.zeros(len(bufs[0].data)))
        for title in titles:
            for corner, coeff in lpy_map[title].items():
                idx = channels.index(get_sus_channel(base_channel, corner))
                lpy[title] += (bufs[idx].data * coeff)/lpy_lim

        colors = ['#2c7fb8', '#e66101', '#5e3c99']
        offset = -0.5
        fig, axs = plt.subplots(3, sharex=True, figsize=(27,16))
        for ax, dof, color, title in zip(axs, lpy.values(), colors, titles):
            ax.plot(
                t,
                dof,
                color = color,
                label = title,
                alpha = 0.8,
                lw = 2
            )
            ax.axhline(
                1,
                color='red',
                linestyle='--',
                lw=3,
                label = 'saturation threshold'
            )
            ax.axhline(
                -1,
                color='red',
                linestyle='--',
                lw=3
            )

            # offset = window[0] / 6 # window dependent offset from lock loss
            offset = -0.5

            ax.set_ylim(-1.2, 1.2)
            ax.set_xlim(window[0], window[1])
            ax.set_xlabel(
                'Time [s] since lock loss at {}'.format(gps),
                labelpad=10,
            )
            ax.grid()
            ax.legend()

        fig.text(0.04, 0.5, r"Counts", ha='center', va='center', rotation='vertical')
        fig.subplots_adjust(hspace=0)
        plt.setp([ax.get_xticklabels() for ax in fig.axes[:-1]], visible=False)

        # set title
        axs[0].set_title("Length-Pitch-Yaw Plots for {}".format(base_channel))

        outfile_plot = 'lpy_{}.png'.format(window_type)
        outpath_plot = event.path(outfile_plot)
        fig.savefig(outpath_plot, bbox_inches='tight')
        fig.clf()


def get_sus_channel(base_channel, corner):
    return '{}_{}_DQ'.format(base_channel, corner)


def extract_sus_channel(channel):
    """Extract optic, stage, corner info out of a SUS channel name.

    """
    prefix, name = channel.split('-', 1)
    optic, stage, _, _, corner, _ = name.split('_', 5)
    return optic, stage, corner


def channel2lpy_coeffs(channel):
    """From a suspension channel, get a dictionary containing the LPY coefficients.

    """
    lpy_map = {}
    optic, stage, corner = extract_sus_channel(channel)

    if optic in ['ETMX', 'ETMY', 'ITMX', 'ITMY']:
        if stage in ['M0']:
            lpy_map['length'] = {'F2': -1, 'F3': -1}
            lpy_map['pitch'] = {'F2': 1, 'F3': 1}
            lpy_map['yaw'] = {'F2': 1, 'F3': -1}
            lpy_map['corners'] = ['F2', 'F3']
        elif stage in ['L1', 'L2', 'L3']:
            lpy_map['length'] = {'UL': 1, 'LL': 1, 'UR': 1, 'LR': 1}
            lpy_map['pitch'] = {'UL': 1, 'LL': -1, 'UR': 1, 'LR': -1}
            lpy_map['yaw'] = {'UL': -1, 'LL': -1, 'UR': 1, 'LR': 1}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['MC1', 'MC2', 'MC3', 'PRM', 'PR2', 'PR3', 'SRM', 'SR2', 'SR3']:
        if stage in ['M1']:
            lpy_map['length'] = {'LF': 1, 'RT': 1}
            lpy_map['pitch'] = {'T2': 1, 'T3': -1}
            lpy_map['yaw'] = {'LF': -1, 'RT': 1}
            lpy_map['corners'] = ['T2', 'T3', 'LF', 'RT']
        elif stage in ['M2', 'M3']:
            lpy_map['length'] = {'UL': 1, 'LL': 1, 'UR': 1, 'LR': 1}
            lpy_map['pitch'] = {'UL': 1, 'LL': -1, 'UR': 1, 'LR': -1}
            lpy_map['yaw'] = {'UL': -1, 'LL': -1, 'UR': 1, 'LR': 1}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['BS']:
        if stage in ['M1']:
            lpy_map['length'] = {'LF': 1, 'RT': 1}
            lpy_map['pitch'] = {'F2': -1, 'F3': -1}
            lpy_map['yaw'] = {'LF': -1, 'RT': 1}
            lpy_map['corners'] = ['F2', 'F3']
        elif stage in ['M2']:
            lpy_map['length'] = {'UL': 1, 'LL': 1, 'UR': 1, 'LR': 1}
            lpy_map['pitch'] = {'UL': 1, 'LL': -1, 'UR': 1, 'LR': -1}
            lpy_map['yaw'] = {'UL': -1, 'LL': -1, 'UR': 1, 'LR': 1}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['OMC']:
        if stage in ['M1']:
            lpy_map['length'] = {'LF': 1, 'RT': 1}
            lpy_map['pitch'] = {'T2': 1, 'T3': -1}
            lpy_map['yaw'] = {'LF': -1, 'RT': 1}
            lpy_map['corners'] = ['T2', 'T3', 'LF', 'RT']
    else:
        raise RuntimeError("no LPY map for suspension '{}'.".format(channel))

    return lpy_map
