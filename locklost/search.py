import os
import argparse
import logging
import importlib

from gwpy.segments import Segment, SegmentList

from . import set_signal_handlers
from . import config
from . import data
from .event import LocklossEvent
from . import segments
from . import condor

##################################################

def search_buf(buf, previous=None, event_callback=None):
    """Search for lock lock events in buffer

    Creates event directory in config.EVENT_ROOT, and note state
    transition index.  Execute event_callback for each found event.

    Returns the number of events found in the buffer.

    """
    lockloss_indices  = [s[0] for s in config.GRD_LOCKLOSS_STATES]
    nevents = 0
    for time, pval, val in data.gen_transitions(buf, previous):
        trans = (int(pval), int(val))
        logging.debug("transition: {:0.3f} {}->{}".format(time, trans[0], trans[1]))
        if val not in lockloss_indices:
            continue
        logging.info("lockloss found: {} {}->{}".format(time, *trans))
        nevents += 1
        try:
            event = LocklossEvent.create(time, trans)
        except OSError as e:
            logging.info(e)
            event = LocklossEvent(time)
        if event_callback:
            logging.info("executing event callback: {}({})".format(
                event_callback.__name__, event.id))
            event_callback(event)
    return nevents


def search(segment):
    """Search segment for events

    """
    logging.debug("searching segment {}...".format(segment))
    channel = config.GRD_STATE_N_CHANNEL
    buf = data.fetch([channel], segment)[0]
    nevents = search_buf(buf)
    segments.write_segments(config.SEG_DIR, [(int(buf.gps_start), int(buf.gps_stop))])
    logging.info("{} events found".format(nevents))


def search_iterate(segment=None, event_callback=None, stat_file=None):
    """Iterative search for events (NDS-only)

    """
    if segment:
        logging.info("searching segment {}...".format(segment))
    else:
        logging.info("searching online...")
    previous = None
    nevents = 0
    nbufs = 0
    progress = None
    channel = config.GRD_STATE_N_CHANNEL
    for bufs in data.nds_iterate([channel], start_end=segment):
        buf = bufs[0]
        nevents += search_buf(
            buf,
            previous=previous,
            event_callback=event_callback,
        )
        previous = buf
        nbufs += 1
        if not progress:
            progress = (int(buf.gps_start), int(buf.gps_stop))
        else:
            progress = (progress[0], int(buf.gps_stop))
        if nbufs % 100 == 0:
            segments.write_segments(config.SEG_DIR, [progress])
            progress = None
        if stat_file:
            os.utime(stat_file, None)
    # FIXME: if the segment is None and we've broken out of the
    # iteration then there was clearly some problem so we throw an
    # error.  NDS should be throwing an exception here.
    if segment is None:
        raise RuntimeError("NDS iteration returned unexpectedly.")
    else:
        logging.info("{} events found".format(nevents))
        # segments.compress_segdir(config.SEG_DIR)

##################################################

def _parser_add_arguments(parser):
    from .util import GPSTimeParseAction
    parser.add_argument('start', action=GPSTimeParseAction,
                        help="search start time")
    parser.add_argument('end', action=GPSTimeParseAction,
                        help="search end time")
    cgroup = parser.add_mutually_exclusive_group()
    cgroup.add_argument('--condor', action='store_true',
                        help="condor search for events")
    cgroup.add_argument('--callback', type=str,
                        help="event callback function")


def main(args=None):
    """Search for lockloss events

    By default an iterative NDS search of the specified span will be
    directly executed.  If the --condor flag is given the specified
    span will be broken up into smaller segments and submitted as a
    single condor DAG to the cluster.

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    start = int(args.start)
    end = int(args.end)

    assert start < end, "start must be before end"

    full_seg = Segment(start, end)

    if args.condor:
        segments.compress_segdir(config.SEG_DIR)
        completed_segs = segments.load_segments(config.SEG_DIR)
        reduced_segs = SegmentList([full_seg]) - completed_segs
        if not reduced_segs:
            logging.info("All segments analyzed")
            raise SystemExit()
        logging.debug("segments: {}".format(reduced_segs))

        def condor_args_gen():
            for s in segments.slice_segments(
                    seglist=reduced_segs,
                    stride=config.SEARCH_STRIDE,
                    overlap=1,
            ):
                yield [
                    ('start', s[0]),
                    ('end', s[1]),
                ]

        dag = condor.CondorDAG(
            config.CONDOR_SEARCH_DIR,
            'search',
            condor_args_gen,
            log='logs/$(start)-$(end).',
        )
        dag.write()
        dag.submit()

    else:
        if args.callback:
            mpath = args.callback.split('.')
            mod = importlib.import_module('.'+'.'.join(mpath[:-1]), __package__)
            func = mod.__dict__[mpath[-1]]
        else:
            func = None
        search_iterate(
            full_seg,
            event_callback=func,
        )


# direct execution of this module intended for condor jobs
if __name__ == '__main__':
    set_signal_handlers()
    logging.basicConfig(
        level='DEBUG',
        format=config.LOG_FMT,
    )
    parser = argparse.ArgumentParser()
    parser.add_argument('start', type=int,
                        help="search start time")
    parser.add_argument('end', type=int,
                        help="search end time")
    args = parser.parse_args()
    search(Segment(args.start, args.end))
