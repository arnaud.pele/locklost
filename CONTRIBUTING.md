# Developing and contributing to `locklost`

We welcome any help developing the `locklost` project, including
adding and improving the analysis followups, improving the web pages,
etc.  Please contact jameson.rollins@ligo.org to coordinate
development.

Since the analysis runs best on the site LDAS clusters, you should
contact your local sysadmin about getting access to your local
cluster.

The `locklost` project uses [git](https://git-scm.com/) and is hosted
at [git.ligo.org](https://git.ligo.org).  Issues are tracked via the
[GitLab issue
tracker](https://git.ligo.org/jameson.rollins/locklost/issues).


# tracking and downloading the source

Contributions to `locklost` are handled via [GitLab merge
request](https://docs.gitlab.com/ee/user/project/merge_requests/) (see
[making merge requests](#making-merge-requests) below).  It's
therefore advised that you
[fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html)
your own copy of the project to work from:

* https://git.ligo.org/jameson.rollins/locklost/forks/new

Once you've forked the project into your personal gitlab namespace,
[clone](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html)
the repository locally (e.g. in your personal LDAS home directory).
This will allow you to develop and test changes without disturbing the
production run.  Assuming your gitlab account name is
"albert.einstein" you can clone your copy of the project with git like
so:
```shell
$ git clone git@git.ligo.org:albert.einstein/locklost.git
```
Note that it's best to access gitlab via [ssh
keys](https://docs.gitlab.com/ee/ssh/), since that will allow you to
easily push changes back to be merged into the main project.


# running the code

## environment variables

`locklost` looks for a couple environments variables that specify
important configuration parameters:

* `IFO` the interferometer in question (e.g. 'H1' or 'L1')
* `LOCKLOST_EVENT_ROOT` root directory for where events are stored

These variables can either be specified in e.g. your `.bashrc`, or in
a wrapper script, or on the command line when you run the code (see below).

## command line interface

When developing, it's easiest to just run the `locklost` code directly
from a checkout of the source.  To do this,  change directory into
the `locklost` directory that you git cloned, and run the package
as so:
```shell
$ IFO=L1 LOCKLOST_EVENT_ROOT=~/my/event/dir python -m locklost --help
```

## developing for the web

If you're working on the web pages, create a new directory in your in
your personal web space
```shell
$ mkdir -p ~/public_html/lockloss
```

In this web directory create a link to your event directory there (or
just put your event directory there directly):
```shell
$ cd ~/public_html/lockloss
$ ln -s ~/my/event/dir events
```

Finally, create the web cgi script in the web directory.  Create a
file called `index.cgi` with the following contents (with variables
modified for your particular use case):

```shell
#!/bin/bash
export IFO=L1
export LOCKLOST_EVENT_ROOT=~/public_html/events
export LOCKLOST_WEB_ROOT=https://ldas-jobs.ligo-la.caltech.edu/~albert.einstein/lockloss
export PYTHONPATH=~/path/to/source/locklost
exec python -m locklost.web
```
Note the `LOCKLOST_WEB_ROOT` variable, which tells the cgi script where
the base URL of the web pages.

Finally, make the script executable:
```shell
$ chmod 755 ~/public_html/lockloss/index.cgi
```

You should now be able to see the web page at the appropriate URL,
e.g. https://ldas-jobs.ligo-la.caltech.edu/~albert.einstein/lockloss


# making merge requests

All contributions to `locklost` are handled via [GitLab merge
request](https://docs.gitlab.com/ee/user/project/merge_requests/).
The basic process for making a merge request is as follows (this
assumes you have forked the project as specified above, and cloned the
repository from this fork):

0. Checkout the master branch and make sure it's up-to-date:
```shell
$ cd ~/path/to/source/locklost
$ git checkout master
$ git pull --rebase origin master
```

1. Create a new branch in your local checkout for your proposed changes:
```shell
$ git checkout -B my-dev-branch
```

2. Commit all of your changes to this new branch:
```shell
$ git commit ....
```

   Make sure your commit log message is nice and informative, starting
   with a single line summary followed by a paragraph describing the
   changes.  If you're making a commit that fixes a particular bug,
   you can add a line like "closes #XX" at the bottom of commit
   message and the corresponding issue will be automatically closed
   when the patch is merged.  See [automatic issue
   closing](https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html).

3. When you've got all changes ready to go, push your development
   branch to your fork of the project (assuming you cloned from your
   fork of the project as specified above):
```shell
$ git push origin my-dev-branch
```

4. Navigate to your gitlab page for the project and click the "New
   merge request" button.

5. Specify that you would like to submit a merge request for your new
   development branch
   (e.g. source="albert.einstein/locklost:my-dev-branch") to be merged
   with the upstream master branch
   (e.g. target="jameson.rollins/locklost:master").

6. Watch the merge request page for review of your request.


# quick links: 

* [git quick reference](//gitref.org/basic/)
